package com.dev.planr.di

import androidx.room.Room
import com.dev.planr.data.db.TodoDatabase
import com.dev.planr.data.db.labels.LabelsDatabase
import com.dev.planr.data.repo.TodoRepo
import com.dev.planr.data.repo.TodoRepoImpl
import com.dev.planr.data.repo.labels.LabelRepo
import com.dev.planr.data.repo.labels.LabelRepoImpl
import com.dev.planr.ui.todoList.TodoViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module



val applicationModule = module {

    // Room database
    single {
        Room.databaseBuilder(androidApplication(),
            TodoDatabase::class.java,
            getProperty("DB_NAME"))
            .build()
    }

    // TodoDao
    single { get<TodoDatabase>().todoDao() }
    single { get<LabelsDatabase>().labelDao()  }

    // TodoRepository
    factory<TodoRepo> { TodoRepoImpl(get()) }
    factory<LabelRepo> { LabelRepoImpl(get()) }

    // TodoViewModel
    viewModel { TodoViewModel(get()) }
}