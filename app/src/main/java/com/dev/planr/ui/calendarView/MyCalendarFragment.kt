package com.dev.planr.ui.calendarView

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dev.planr.R

class MyCalendarFragment : Fragment(){

    companion object{
        fun newInstance(): MyCalendarFragment = MyCalendarFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_mycalendar, container, false)

}