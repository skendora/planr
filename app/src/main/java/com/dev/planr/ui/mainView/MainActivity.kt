package com.dev.planr.ui.mainView

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.dev.planr.R
import com.dev.planr.data.db.TodoRecord
import com.dev.planr.ui.calendarView.MyCalendarFragment
import com.dev.planr.ui.createTodo.CreateTodoActivity
import com.dev.planr.ui.tasksView.MyTasksFragment
import com.dev.planr.ui.todoList.TodoViewModel
import com.dev.planr.utils.Constants
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.main_activity.*
import org.koin.androidx.viewmodel.ext.viewModel
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    private val todoViewModel: TodoViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        toolbar = main_toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        fab_new_todo.setOnClickListener {

            val intent = Intent(this, CreateTodoActivity::class.java)
            startActivityForResult(intent, Constants.INTENT_CREATE_TODO)
        }

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.task_menu_item -> {
                main_toolbar.title = getString(R.string.my_tasks)
                val myTasksFragment = MyTasksFragment.newInstance()
                openFragment(myTasksFragment)

                return@OnNavigationItemSelectedListener true
            }
            R.id.cal_menu_item -> {
                val cal = Calendar.getInstance()
                val month: String? = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
                main_toolbar.title = month

                val myCalendarFragment = MyCalendarFragment.newInstance()
                openFragment(myCalendarFragment)

                return@OnNavigationItemSelectedListener true
            }
            R.id.settings_menu_item -> {
                main_toolbar.title = getString(R.string.settings)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val todoRecord = data?.getParcelableExtra<TodoRecord>(Constants.INTENT_OBJECT)!!
            when (requestCode) {
                Constants.INTENT_CREATE_TODO -> {
                    todoViewModel.saveTodo(todoRecord)
                }
                Constants.INTENT_UPDATE_TODO -> {
                    todoViewModel.updateTodo(todoRecord)
                }
            }
        }
    }


    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}