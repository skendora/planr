package com.dev.planr.ui

import android.app.Application
import com.dev.planr.di.applicationModule
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.core.context.startKoin


class Planr : Application() {

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)

        startKoin {
            androidContext(this@Planr)
            modules(applicationModule)
            androidFileProperties()
        }
    }
}