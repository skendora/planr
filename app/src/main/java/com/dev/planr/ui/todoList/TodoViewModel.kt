package com.dev.planr.ui.todoList

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dev.planr.data.db.TodoRecord
import com.dev.planr.data.repo.TodoRepo


class TodoViewModel(private val todoRepo: TodoRepo) : ViewModel() {

    fun saveTodo(todo: TodoRecord) {
        todoRepo.saveTodo(todo)
    }

    fun updateTodo(todo: TodoRecord){
        todoRepo.updateTodo(todo)
    }

    fun deleteTodo(todo: TodoRecord) {
        todoRepo.deleteTodo(todo)
    }

    fun getAllTodoList(): LiveData<List<TodoRecord>> {
        return todoRepo.getAllTodoList()
    }

}
