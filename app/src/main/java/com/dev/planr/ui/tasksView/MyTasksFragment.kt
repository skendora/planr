package com.dev.planr.ui.tasksView

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dev.planr.R
import com.dev.planr.ui.todoList.TodoListAdapter
import com.dev.planr.ui.todoList.TodoViewModel
import org.koin.androidx.viewmodel.ext.viewModel
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.dev.planr.data.db.TodoRecord
import com.dev.planr.ui.createTodo.CreateTodoActivity
import com.dev.planr.utils.Constants
import kotlinx.android.synthetic.main.fragment_mytasks.*


class MyTasksFragment: Fragment(), TodoListAdapter.TodoEvents {

    private val todoViewModel: TodoViewModel by viewModel()
    private lateinit var todoAdapter: TodoListAdapter


    companion object{
        fun newInstance(): MyTasksFragment = MyTasksFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_mytasks, container, false)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rv_todo_list.layoutManager = GridLayoutManager(context, 2)
        todoAdapter = TodoListAdapter(this)
        rv_todo_list.adapter = todoAdapter


        //Setting up LiveData
        todoViewModel.getAllTodoList()
                .observe(this, Observer
                {
                    Log.d("TODO List = ", it.size.toString())
                    todoAdapter.setAllTodoItems(it)
                })
    }



    override fun onDeleteClicked(todoRecord: TodoRecord) {
        todoViewModel.deleteTodo(todoRecord)
    }

    override fun onViewClicked(todoRecord: TodoRecord) {
        val intent = Intent(context, CreateTodoActivity::class.java)
        intent.putExtra(Constants.INTENT_OBJECT, todoRecord)
        startActivityForResult(intent, Constants.INTENT_UPDATE_TODO)
    }


}