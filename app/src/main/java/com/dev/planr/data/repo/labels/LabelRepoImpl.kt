package com.dev.planr.data.repo.labels

import androidx.lifecycle.LiveData
import com.dev.planr.data.db.labels.LabelDao
import com.dev.planr.data.db.labels.LabelRecord
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class LabelRepoImpl (private val labelDao: LabelDao) : LabelRepo {
    override fun saveLabels(labelRecord: LabelRecord) {
        runBlocking {
            this.launch(Dispatchers.IO) {
                labelDao.saveLabels(labelRecord)
            }
        }
    }

    override fun deleteLabels(labelRecord: LabelRecord) {
        runBlocking {
            this.launch(Dispatchers.IO) {
                labelDao.deleteLabels(labelRecord)
            }
        }
    }

    override fun updateLabels(labelRecord: LabelRecord) {
        runBlocking {
            this.launch(Dispatchers.IO) {
                labelDao.updateLabels(labelRecord)
            }
        }
    }

    override fun getAllLabels(): LiveData<List<String>> {
           return labelDao.getAllLabels()

    }

}