package com.dev.planr.data.repo.labels

import androidx.lifecycle.LiveData
import com.dev.planr.data.db.labels.LabelRecord

interface LabelRepo {

    fun saveLabels(todo: LabelRecord)
    fun updateLabels(todo: LabelRecord)
    fun deleteLabels(todo: LabelRecord)
    fun getAllLabels(): LiveData<List<String>>
}