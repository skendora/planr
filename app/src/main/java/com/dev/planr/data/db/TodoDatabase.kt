package com.dev.planr.data.db

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [TodoRecord::class], version = 2, exportSchema = false)
abstract class TodoDatabase : RoomDatabase() {

    abstract fun todoDao(): TodoDao

}