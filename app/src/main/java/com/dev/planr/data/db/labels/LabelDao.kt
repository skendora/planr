package com.dev.planr.data.db.labels

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dev.planr.data.db.TodoRecord

@Dao
interface LabelDao {

    @Insert
    suspend fun saveLabels(labelRecord: LabelRecord)

    @Delete
    suspend fun deleteLabels(labelRecord: LabelRecord)

    @Update
    suspend fun updateLabels(labelRecord: LabelRecord)

    @Query("SELECT title FROM label ORDER BY id DESC")
    fun getAllLabels(): LiveData<List<String>>
}