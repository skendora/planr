package com.dev.planr.data.db

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dev.planr.data.db.labels.LabelRecord
import kotlinx.android.parcel.Parcelize


@Entity(tableName = "todo")
@Parcelize
data class TodoRecord(@PrimaryKey(autoGenerate = true) val id: Long?,
                      @ColumnInfo(name = "title") val title: String,
                      @ColumnInfo(name = "label") val label: String,
                      @ColumnInfo(name = "content") val content: String) : Parcelable