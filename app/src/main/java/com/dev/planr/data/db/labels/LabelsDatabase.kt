package com.dev.planr.data.db.labels

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dev.planr.data.db.TodoDao
import com.dev.planr.data.db.TodoRecord

@Database(entities = [LabelRecord::class], version = 1, exportSchema = false)
abstract class LabelsDatabase : RoomDatabase() {

    abstract fun labelDao(): LabelDao

}