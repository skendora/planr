package com.dev.planr.data.repo

import androidx.lifecycle.LiveData
import com.dev.planr.data.db.TodoRecord


interface TodoRepo {
    fun saveTodo(todo: TodoRecord)
    fun updateTodo(todo: TodoRecord)
    fun deleteTodo(todo: TodoRecord)
    fun getAllTodoList(): LiveData<List<TodoRecord>>
}